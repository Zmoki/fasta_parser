#!/usr/bin/python
# -*- coding: UTF-8 -*-

#Знаю, что код жутко корявый, но таки первые тычки в питоне

import ConfigParser
from Bio import SeqIO


def read_config():  #читаем конфиг используя config parser, см. туториал по нему.
    global input_file_name
    global output_file_name
    global file_mode
    global debug
    config = ConfigParser.RawConfigParser()
    config.read('1.cfg')

    input_file_name = config.get('main','input file')
    output_file_name = config.get('main','output file')
    debug = config.getboolean('other','diagnostic')
    file_mode=config.get('other','file mode')

    if debug == True:
        print "DEBUG MODE"
        print "Input file:", input_file_name
        print "Output file:", output_file_name


def read_fasta():
    global out_file_name
    global database_type
    global ser_number
    global seq_name
    global seq_base
    i=0
    database_type={}
    ser_number={}
    seq_name={}
    seq_base={}
    fasta_sequences = SeqIO.parse(open(input_file_name),'fasta')
    for fasta in fasta_sequences:
        i=i+1
        name, sequence, description = fasta.id, fasta.seq.tostring(), fasta.description
        db_temp = name.split("|",5)[2] #режем строки по символу |
        sn_temp = name.split("|",5)[3]
        desc_temp = description.split("|", 5)[4]
        desc_temp = desc_temp.split(",",1)[0]
        desc_temp = desc_temp.strip()
        database_type.update({i:db_temp}) #добавляем в листы
        ser_number.update({i:sn_temp})
        seq_name.update({i:desc_temp})
        seq_base.update({i:sequence})
    if debug == True:
        print ("\nFound %s sequences." %len(seq_base))
    return




def write_fasta():
    i=0
    f = open(output_file_name, file_mode)
    for i in seq_name:
        f.write(">%s_%s_%s \n" % (database_type[i], ser_number[i], seq_name[i]))
        f.write("%s \n" % seq_base[i])
        f.write("\n")
    f.close()



if  __name__=='__main__':
    print "Hello! this is simple FASTA parser."
    read_config()
    read_fasta()
    write_fasta()
    print "\nProcess complete! Check it out, please."
    print "Goodbye!"
